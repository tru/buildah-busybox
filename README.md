# building a busybox toy system docker image with buildah for CI

Tru <tru@pasteur.fr>

## Why ?
- toy system for gitlab-CI
- build docker image from dockerhub registry and push to registry.pasteur.fr with proper tags
- using buildah instead of Docker IN Docker (dind)
- https://major.io/2019/05/24/build-containers-in-gitlab-ci-with-buildah/ and https://blog.revolve.team/2021/07/20/build-images-docker-gitlab-ci-sans-dind/
## Caveat
- playground, use at your own risk!
- `:main` tagged docker image

## Usage
```
docker run -ti registry-gitlab.pasteur.fr/tru/buildah-busybox:latest
```
```
singularity run docker://registry-gitlab.pasteur.fr/tru/buildah-busybox:latest
```
